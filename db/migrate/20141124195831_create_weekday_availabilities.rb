class CreateWeekdayAvailabilities < ActiveRecord::Migration
  def change
    create_table :weekday_availabilities do |t|
      t.string :w_day

      t.timestamps
    end
  end
end
