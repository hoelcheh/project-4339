class CreateDiagnosticCodes < ActiveRecord::Migration
  def change
    create_table :diagnostic_codes do |t|
      t.integer :code_number
      t.string :code_name
      t.float :cost

      t.timestamps
    end
  end
end
