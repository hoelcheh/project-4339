class CreatePhysicianAvailabilities < ActiveRecord::Migration
  def change
    create_table :physician_availabilities do |t|
      t.string :status

      t.timestamps
    end
  end
end
