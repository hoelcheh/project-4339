require 'test_helper'

class PhyAvailabilitiesControllerTest < ActionController::TestCase
  setup do
    @phy_availability = phy_availabilities(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:phy_availabilities)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create phy_availability" do
    assert_difference('PhyAvailability.count') do
      post :create, phy_availability: @phy_availability.attributes
    end

    assert_redirected_to phy_availability_path(assigns(:phy_availability))
  end

  test "should show phy_availability" do
    get :show, id: @phy_availability
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @phy_availability
    assert_response :success
  end

  test "should update phy_availability" do
    put :update, id: @phy_availability, phy_availability: @phy_availability.attributes
    assert_redirected_to phy_availability_path(assigns(:phy_availability))
  end

  test "should destroy phy_availability" do
    assert_difference('PhyAvailability.count', -1) do
      delete :destroy, id: @phy_availability
    end

    assert_redirected_to phy_availabilities_path
  end
end
