class Physician < ActiveRecord::Base

  belongs_to :physician_availability
  has_many :appointments
  has_many :patients, through: :appointments

end
