class PhysicianAvailabilitiesController < ApplicationController
  # GET /physician_availabilities
  # GET /physician_availabilities.json
  def index
    @physician_availabilities = PhysicianAvailability.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @physician_availabilities }
    end
  end

  # GET /physician_availabilities/1
  # GET /physician_availabilities/1.json
  def show
    @physician_availability = PhysicianAvailability.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @physician_availability }
    end
  end

  # GET /physician_availabilities/new
  # GET /physician_availabilities/new.json
  def new
    @physician_availability = PhysicianAvailability.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @physician_availability }
    end
  end

  # GET /physician_availabilities/1/edit
  def edit
    @physician_availability = PhysicianAvailability.find(params[:id])
  end

  # POST /physician_availabilities
  # POST /physician_availabilities.json
  def create
    @physician_availability = PhysicianAvailability.new(params[:physician_availability])

    respond_to do |format|
      if @physician_availability.save
        format.html { redirect_to @physician_availability, notice: 'Physician availability was successfully created.' }
        format.json { render json: @physician_availability, status: :created, location: @physician_availability }
      else
        format.html { render action: "new" }
        format.json { render json: @physician_availability.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /physician_availabilities/1
  # PUT /physician_availabilities/1.json
  def update
    @physician_availability = PhysicianAvailability.find(params[:id])

    respond_to do |format|
      if @physician_availability.update_attributes(params[:physician_availability])
        format.html { redirect_to @physician_availability, notice: 'Physician availability was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @physician_availability.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /physician_availabilities/1
  # DELETE /physician_availabilities/1.json
  def destroy
    @physician_availability = PhysicianAvailability.find(params[:id])
    @physician_availability.destroy

    respond_to do |format|
      format.html { redirect_to physician_availabilities_url }
      format.json { head :no_content }
    end
  end
end
