class WeekdayAvailabilitiesController < ApplicationController


  # GET /weekday_availabilities
  # GET /weekday_availabilities.json
  def index
    @weekday_availabilities = WeekdayAvailability.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @weekday_availabilities }
    end
  end

  # GET /weekday_availabilities/1
  # GET /weekday_availabilities/1.json
  def show
    @weekday_availability = WeekdayAvailability.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @weekday_availability }
    end
  end

  # GET /weekday_availabilities/new
  # GET /weekday_availabilities/new.json
  def new
    @weekday_availability = WeekdayAvailability.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @weekday_availability }
    end
  end

  # GET /weekday_availabilities/1/edit
  def edit
    @weekday_availability = WeekdayAvailability.find(params[:id])
  end

  # POST /weekday_availabilities
  # POST /weekday_availabilities.json
  def create
    @weekday_availability = WeekdayAvailability.new(params[:weekday_availability])

    respond_to do |format|
      if @weekday_availability.save
        format.html { redirect_to @weekday_availability, notice: 'Weekday availability was successfully created.' }
        format.json { render json: @weekday_availability, status: :created, location: @weekday_availability }
      else
        format.html { render action: "new" }
        format.json { render json: @weekday_availability.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /weekday_availabilities/1
  # PUT /weekday_availabilities/1.json
  def update
    @weekday_availability = WeekdayAvailability.find(params[:id])

    respond_to do |format|
      if @weekday_availability.update_attributes(params[:weekday_availability])
        format.html { redirect_to @weekday_availability, notice: 'Weekday availability was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @weekday_availability.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /weekday_availabilities/1
  # DELETE /weekday_availabilities/1.json
  def destroy
    @weekday_availability = WeekdayAvailability.find(params[:id])
    @weekday_availability.destroy

    respond_to do |format|
      format.html { redirect_to weekday_availabilities_url }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_weekday_availability
    @weekday_availability = WeekdayAvailability.find(params[:id])
  end

  def slot_params
    params.require(:slot).permit(:time_block)
  end
end

